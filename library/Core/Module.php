<?php
namespace Core;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventInterface;
use Zend\Http\Response;
use Zend\ServiceManager\ServiceManager;
use Core\Acl\Service\Acl;
use Core\Listeners\AclListener;
use Core\Acl\Model\RulesDao;
use Core\Acl\Model\RulesMapper;
use Core\Acl\Model\Resource as AclResource;
use Core\Acl\Model\Role as AclRole;
use Core\Acl\Model\Rule as AclRule;
use Zend\Mvc\MvcEvent;

abstract class Module implements AutoloaderProviderInterface, BootstrapListenerInterface, ConfigProviderInterface, ServiceProviderInterface
{

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ModuleManager\Feature\ServiceProviderInterface::getServiceConfig()
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Core/Acl/Service/Acl' => function (\Zend\ServiceManager\ServiceManager $serviceManager)
                {
                    $acl = new Acl($serviceManager->get('Core/Acl/Model/RulesDao'));
                    return $acl;
                },
                'Core/Acl/Service' => function (\Zend\ServiceManager\ServiceManager $serviceManager)
                {
                    $config = $serviceManager->get('config');
                    $authorizeClass = $config['Acl']['authorize_provider'];
                    return new $authorizeClass();
                },
                'Core/Acl/Model/RulesDao' => function (\Zend\ServiceManager\ServiceManager $serviceManager)
                {
                    $rulesMapper = $serviceManager->get('Core/Acl/Model/RulesMapper');
                    return new RulesDao($rulesMapper);
                },
                'Core/Acl/Model/RulesMapper' => function (\Zend\ServiceManager\ServiceManager $serviceManager)
                {
                    $config = $serviceManager->get('config');
                    return new RulesMapper($config['Acl']['data']);
                },
                'Logger' => function($sm){
                    $logger = new \Zend\Log\Logger;
                    $writer = new \Zend\Log\Writer\Stream('./data/log/'.date('Y-m-d').'-error.log');
                    $logger->addWriter($writer);
                }
            )
        );
    }

    /**
     * Attaches the ApiErrorListener on the render event
     *
     * @param EventInterface $event            
     */
    public function onBootstrap(EventInterface $event)
    {        
        
        $application = $event->getTarget();
        $serviceManager = $application->getServiceManager();
        $events = $application->getEventManager();
        $events->attach(new \Core\Listeners\ApiErrorListener());
        //$events->attach(new \Core\Listeners\AclListener());

    }

    /**
     * Convenience method to return the config file
     *
     * @return array
     */
    public function getConfig()
    {
        return include $this->getModulePath() . '/config/module.config.php';
    }

    /**
     * Convenience method to return the acl file
     *
     * @return array
     */
    public function getAclConfig()
    {
        return include $this->getModulePath() . '/config/autoload/acl.global.php';
    }

    /**
     * Return an autoloader configured namespace
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    $this->getNamespace() => $this->getModulePath() . '/src/' . $this->getNamespace()
                )
            )
        );
    }

    /**
     * Will get the path based on the class
     *
     * @return string
     */
    private function getModulePath()
    {
        $reflector = new \ReflectionClass(get_class($this));
        $filename = $reflector->getFileName();
        return dirname($filename);
    }

    /**
     * Will get the namespace based on the class
     *
     * @return string
     */
    private function getNamespace()
    {
        $class = get_called_class();
        $lastSlashPosition = strrpos($class, '\\');
        $namespace = substr($class, 0, $lastSlashPosition);
        return $namespace;
    }
}