<?php
namespace Core\Listeners;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\Http\Response;

/**
 * Listener attached to render to check the response.
 * If the response contains an error a JSON model will
 * be returned containing the error followinf the
 * api problem API
 *
 * @package Core\Listeners
 */
class ApiErrorListener extends AbstractListenerAggregate
{

    /**
     * Method to register this listener on the render event
     *
     * @param EventManagerInterface $events            
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_RENDER, __CLASS__ . '::onRender', 0);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, __CLASS__ . '::onError', 0);
    }

    /**
     * Implement Zend log or something similar
     * Just log for now.
     *
     * @param MvcEvent $e            
     */
    public static function onError(MvcEvent $e)
    {
        $exception = $e->getParam("exception");

        if (!$exception) {
            return;
        }
        
        $model = new JsonModel(array(
            'errorCode' => $exception->getCode(),
            'errorMsg' => $exception->getMessage()
        ));
        
        $model->setTerminal(true);
        
        $e->setResult($model);
        $e->setViewModel($model);
        $e->getResponse()->setStatusCode($exception->getCode());
        
        
        error_log($e->getParam("exception"));

    }

    /**
     * Method executed when the render event is triggered
     *
     * @param MvcEvent $e            
     * @return void
     */
    public static function onRender(MvcEvent $e)
    {
        if ($e->getRequest() instanceof \Zend\Console\Request || $e->getResponse()->isOk() || $e->getResponse()->getStatusCode() == Response::STATUS_CODE_401) {
            return;
        }

        $exception = $e->getParam("exception");
        
        
        $httpCode = $e->getResponse()->getStatusCode();
        $model = new JsonModel(array(
            'errorCode' => $httpCode,
            'errorMsg' => $exception?$exception->getMessage():$e->getResponse()->getReasonPhrase()
        ));
        
        $model->setTerminal(true);
        
        $e->setResult($model);
        $e->setViewModel($model);
        $e->getResponse()->setStatusCode($httpCode);
    }
}
