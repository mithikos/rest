<?php
namespace Core\Listeners;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;

use \Core\Acl\Service\Acl as Acl;
use \Core\Acl\Model\Resource as AclResource;
use \Core\Acl\Model\Role as AclRole;
use \Core\Acl\Model\Rule as AclRule;

class AclListener implements  ListenerAggregateInterface
{
	protected $_listeners = array();

	/**
	 * Attach one or more listeners
	 *
	 * Implementors may add an optional $priority argument; the EventManager
	 * implementation will pass this to the aggregate.
	 *
	 * @param EventManagerInterface $events
	 */
	public function attach(EventManagerInterface $events)
	{
		$this->_listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'initAcl'), 100);
	}

	/**
	 * Detach all previously attached listeners
	 *
	 * @param EventManagerInterface $events
	 */
	public function detach(EventManagerInterface $events)
	{
		foreach ($this->_listeners as $index => $listener) {
			if ($events->detach($listener)) {
				unset($this->_listeners[$index]);
			}
		}
	}

	public function initAcl(MvcEvent $e)
	{
		/* @var \Zend\Mvc\Application $app */
		$app = $e->getApplication();

		// Get SM
		$sm = $app->getServiceManager();

		/* @var Acl $acl*/
		$acl = $sm->get('Core/Acl/Service/Acl');

		// Get params 'controller', 'action' and 'privilege' from route match
		$matches = $e->getRouteMatch();

		// Resource based on request params
		$resource = new AclResource();
		$resource->setController($matches->getParam('controller'));
		$resource->setAction($matches->getParam('action', 'index'));

		// 404 response if resource does not exist
		if (!$acl->hasResource($resource, true)) {
			$e->getResponse()->setStatusCode(404);
			return;
		}

		// Get config 
		$config = $sm->get('config');
		$configAcl = $config['Acl'];

		// Role
		$auth = $sm->get('Core/Acl/Service');

		$role = new AclRole();
		$role->setName($auth->hasIdentity() ?
			$auth->getIdentity()->$configAcl['field_role'] :
				$configAcl['default_role']
		);

		// Query ACL
		$result = $acl->isAllowed($role, $resource, $e->getRequest()->getMethod());

		// 403 Unauthorized
		if ($result === false) {
			// Create ViewModel
			$model = new \Zend\View\Model\JsonModel();
			$model->setVariable('exception', new \Exception(Acl::ERROR_UNAUTHORIZED));
            
			$e->getResponse()->setReasonPhrase(Acl::ERROR_UNAUTHORIZED);
			// Add $model as a child and set 403 status code
			$e->getResponse()->setStatusCode(403);

			// Stop propagation
			$e->stopPropagation();
			return;
		}
	}
}
