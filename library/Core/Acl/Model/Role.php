<?php
namespace Core\Acl\Model;

use Zend\Permissions\Acl\Role\RoleInterface;

class Role implements RoleInterface
{

    /**
     *
     * @var int
     */
    protected $_id;

    /**
     *
     * @var string
     */
    protected $_name;

    /**
     *
     * @var Role[]|null
     */
    protected $_parents;

    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data)
    {
        $this->setId((isset($data['id'])) ? $data['id'] : null);
        $this->setName((isset($data['name'])) ? $data['name'] : null);
        $this->setParents((isset($data['parents'])) ? $data['parents'] : null);
    }

    /**
     * 
     * @param \Core\Acl\Model\Role[] $parents
     */
    public function setParents($parents)
    {
        $this->_parents = $parents;
    }

    /**
     * 
     * @return \Core\Acl\Model\Role[]
     */
    public function getParents()
    {
        return $this->_parents;
    }

    /**
     * 
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * 
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * 
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Returns the string identifier of the Role
     *
     * @return string
     */
    public function getRoleId()
    {
        return $this->getName();
    }
}
