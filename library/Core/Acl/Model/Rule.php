<?php
namespace Core\Acl\Model;

use Core\Acl\Model\Role as AclRole;
use Core\Acl\Model\Resource as AclResource;

class Rule
{
    // Privilege
    const PRIVILEGE_GET = 'GET';
    const PRIVILEGE_POST = 'POST';
    const PRIVILEGE_PUT = 'PUT';
    const PRIVILEGE_DELETE = 'DELETE';
    const PRIVILEGE_HEAD = 'HEAD';
    const PRIVILEGE_TRACE = 'TRACE';
    const PRIVILEGE_OPTIONS = 'OPTIONS';
    
    
    /**
     * 
     * @var int
     */
    protected $_id;

    protected $_permission;

    /**
     * 
     * @var unknown
     */
    protected $_privileges;

    /**
     * 
     * @var int
     */
    protected $_active;

    /**
     *
     * @var \Core\Core\Acl\Model\Role
     */
    protected $_role;

    /**
     *
     * @var \Core\Acl\Model\Resource
     */
    protected $_resource;
    

    protected $_allowPrivileges = array(
        self::PRIVILEGE_GET,
        self::PRIVILEGE_POST,
        self::PRIVILEGE_PUT,
        self::PRIVILEGE_DELETE,
        self::PRIVILEGE_HEAD,
        self::PRIVILEGE_TRACE,
        self::PRIVILEGE_OPTIONS
    );

    /**
     * 
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->setId((isset($data['id'])) ? $data['id'] : null);
        $this->setPermission((isset($data['permission'])) ? $data['permission'] : null);
        $this->setPrivileges((isset($data['privilege'])) ? $data['privilege'] : null);
        $this->setActive((isset($data['active'])) ? $data['active'] : null);
        
        if (isset($data['role'])) {
            $role = new AclRole();
            $role->exchangeArray(array(
                'role' => $data['role']
            ));
            $this->setRole($role);
        } else {
            $this->setRole(null);
        }
        
        if (isset($data['controller'])) {
            $resource = new AclResource();
            $resource->exchangeArray(array(
                'controller' => $data['controller'],
                'action' => $data['action']
            ));
            
            $this->setResource($resource);
        } else {
            $this->setResource(null);
        }
    }

    /**
     * Return if the rule is active or not
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->getActive() ? true : false;
    }

    /**
     * 
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * 
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * 
     * @param unknown $permission
     */
    public function setPermission($permission)
    {
        $this->_permission = $permission;
    }

    /**
     * 
     * @return unknown
     */
    public function getPermission()
    {
        return $this->_permission;
    }

    /**
     * 
     * @param array $privileges
     * @throws \Exception
     */
    public function setPrivileges(array $privileges)
    {
        foreach ($privileges as $item) {
            if (! in_array($item, $this->_allowPrivileges)) {
                throw new \Exception("'$item' is not in the allow list :" . var_export($this->_allowPrivileges, true));
            }
        }
        
        $this->_privileges = $privileges;
    }

    /**
     * 
     * @return \Core\Acl\Model\
     */
    public function getPrivileges()
    {
        return $this->_privileges;
    }

    /**
     *
     * @param \Core\Acl\Model\Resource $resource            
     */
    public function setResource($resource)
    {
        $this->_resource = $resource;
    }

    /**
     *
     * @return \Core\Acl\Model\Resource
     */
    public function getResource()
    {
        return $this->_resource;
    }

    /**
     *
     * @param \Core\Acl\Model\Role $role            
     */
    public function setRole($role)
    {
        $this->_role = $role;
    }

    /**
     * Returns aggregation (of roles)
     *
     * @return \Core\Acl\Model\Role
     */
    public function getRole()
    {
        return $this->_role;
    }

    /**
     * 
     * @param int $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * 
     * @return int
     */
    public function getActive()
    {
        return $this->_active;
    }
}
