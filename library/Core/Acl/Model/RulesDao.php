<?php
namespace Core\Acl\Model;

use Core\Acl\Model\RulesMapper;

class RulesDao
{

    /**
     *
     * @var RulesMapper
     */
    protected $_dataMapper;

    /**
     * 
     * @param RulesMapper $dataMapper
     */
    public function __construct(RulesMapper $dataMapper)
    {
        $this->_dataMapper = $dataMapper;
    }

    /**
     * 
     * @param unknown $type
     */
    public function findByType($type)
    {
        return $this->_dataMapper->findRulesByType($type);
    }

    /**
     * 
     */
    public function findRoles()
    {
        return $this->_dataMapper->findRoles();
    }

    /**
     * 
     */
    public function findResources()
    {
        return $this->_dataMapper->findResources();
    }
}
