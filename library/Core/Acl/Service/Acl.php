<?php
namespace Core\Acl\Service;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\EventManager\EventManager;
use Core\Acl\Model\Role as AclRole;
use Core\Acl\Model\Resource as AclResource;
use Core\Acl\Model\Rule as AclRule;
use Core\Acl\Model\RulesDao;

class Acl extends ZendAcl
{

    const ERROR_UNAUTHORIZED = 'error-unauthorized';

    /**
     *
     * @var EventManager
     */
    protected $_eventManager;

    /**
     *
     * @var RulesDao
     */
    protected $_rulesDao;

    /**
     * Initialize roles, resources and rules
     *
     * @param \Core\Acl\Model\RulesDao $rulesDao            
     */
    public function __construct(RulesDao $rulesDao)
    {
        // Hold RulesDao
        $this->_rulesDao = $rulesDao;
        // add roles
        foreach ($this->getRulesDao()->findRoles() as $role) {
            /* @var AclRole $role */
            $this->addRole($role, $role->getParents());
        }
        // add resources
        foreach ($this->getRulesDao()->findResources() as $resource) {
            /* @var AclResource $resource */
            $this->addResource($resource, $resource->getParents());
        }
        
        // Rules allow
        foreach ($this->getRulesDao()->findByType('allow') as $rule) {
            /* @var AclRule $rule */
            $this->allow($rule->getRole()
                ->getRoleId(), $rule->getResource() ? $rule->getResource()
                ->getResourceId() : null, $rule->getPrivileges());
        }
        
        // Rules deny
        foreach ($this->getRulesDao()->findByType('deny') as $rule) {
            /* @var AclRule $rule */
            $this->deny($rule->getRole()
                ->getRoleId(), $rule->getResource() ? $rule->getResource()
                ->getResourceId() : null, $rule->getPrivileges());
        }
    }

    /**
     * Override in order to use lower case
     *
     * @param null|string|AclRole $role            
     * @param null|string|AclResource $resource            
     * @param null|string $privilege            
     * @return bool
     */
    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        // Check is resource exists
        if (! $this->hasResource($resource, true)) {
            return false;
        }
        
        // Return
        return parent::isAllowed($role, $resource, $privilege);
    }

    /**
     * Returns true if and only if the Resource exists in the ACL
     *
     * Resource can be downgrade optionally
     *
     * @param AclResource $resource            
     * @param bool $downgrade            
     * @return bool
     */
    public function hasResource($resource, $downgrade = false)
    {
        // Exist $resource?
        $result = parent::hasResource($resource);
        
        // Return when $resource exist or cannot downgrade
        if ($result === true || $downgrade === false) {
            return $result;
        }
        
        // Downgrade $resource
        $resource->downgrade();
        
        // Try again with $resource downgrade
        return parent::hasResource($resource);
    }

    /**
     *
     * @return \Core\Acl\Model\RulesDao
     */
    public function getRulesDao()
    {
        return $this->_rulesDao;
    }
}
