<?php
namespace OrdersTest\Controller;

use OrdersTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Orders\Controller\IndexController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
// use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{

    protected $controller;

    protected $request;

    protected $response;

    protected $routeMatch;

    protected $event;

    protected static $order;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new IndexController();
        $this->request = new Request();
        
        $this->routeMatch = new RouteMatch(array(
            'controller' => 'index'
        ));
        
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }

    public function testCreateCanBeAccessed()
    {
        $this->request->setMethod('post');
        $result = $this->controller->dispatch($this->request);
        static::$order = $result->getVariables();
    
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    
    public function testGetListCanBeAccessed()
    {
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetCanBeAccessed()
    {
        $this->routeMatch->setParam('id', static::$order['order_id']);
        $result = $this->controller->dispatch($this->request);
        $obj = $result->getVariables();
        $this->assertArrayHasKey('order_id', $obj);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testStatusPlace() {

        //create a order
        $this->assertArrayHasKey('order_id', static::$order);
        $this->routeMatch->setParam('id', static::$order['order_id']);
        $this->request->setMethod('put');
        $this->request->setContent("status=place");
        try {
            $result = $this->controller->dispatch($this->request);
            $response = $this->controller->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
        }catch(\Exception $ex){
            return;
        }
        
        $this->fail("Can place order without items.");
    }
    
    public function testDeleteCanBeAccessed()
    {
        
        $this->assertArrayHasKey('order_id', static::$order);
        
        $this->routeMatch->setParam('id', static::$order['order_id']);
        $this->request->setMethod('delete');
        try {
            $result = $this->controller->dispatch($this->request);
            $response = $this->controller->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
        } catch (\Exception $ex) {
            return;
        }
        
        $this->fail("Cannot delete");
    }
}