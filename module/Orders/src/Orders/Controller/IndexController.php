<?php
namespace Orders\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * This class is the responsible to answer the requests to the /wall endpoint
 *
 * @package Orders/Controller
 */
class IndexController extends AbstractRestfulController
{

    /**
     * Holds the order model
     * @var \Orders\Model\Order
     */
    private $orders;

    /**
     * Holds all the available status
     * @var array
     */
    private $availableStatus = array(
        "cancel",
        "placed",
        "paid"
    );

    /**
     * Method not available for this endpoint
     *
     * @return void
     */
    public function get($id)
    {
        $order = $this->getOrdersModel()->getOrderById($id);
        return new JsonModel($order);
    }

    /**
     * Will get a list based on status or all
     *
     * @return void
     */
    public function getList()
    {
        $orders = $this->getOrdersModel();
        
        $status = $this->params()->fromRoute('status');
        
        if (isset($status)) {
            $records = $orders->getOrdersByStatus($status);
        } else {
            $records = $orders->getAllOrders();
        }
        
        return new JsonModel($records);
    }

    /**
     * This method inspects the request and routes the data
     * to the correct method
     *
     * @return void
     */
    public function create($data)
    {
        $orders = $this->getOrdersModel();
        $order = $orders->create($data);
        return new JsonModel($order);
    }

    /**
     * Changes the status of the order and provide info
     *
     * @return void
     */
    public function update($id, $data)
    {
        $orders = $this->getOrdersModel();
        $order = $orders->update($id, $data);
        return new JsonModel($order);
    }

    /**
     * Return the orders model
     * 
     * @return \Orders\Model\Order
     */
    public function getOrdersModel()
    {
        if (! $this->orders) {
            $this->orders = $this->getServiceLocator()->get('Orders\Model\Order');
        }
        
        return $this->orders;
    }
}