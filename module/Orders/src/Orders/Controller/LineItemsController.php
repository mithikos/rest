<?php
namespace Orders\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * This class is the responsible to answer the requests to the /wall endpoint
 *
 * @package Orders/Controller
 */
class LineItemsController extends AbstractRestfulController
{

    /**
     * 
     * @var \Orders\Model\Order
     */
    private $orders;
    
    /**
     * 
     * @var \Orders\Model\LineItem
     */
    private $lineItems;
    
    /**
     * Fetch a list of products for this order
     *
     * @param int $id
     *            The order id
     * @return void
     */
    public function get($id)
    {
        $order = $this->getOrdersModel()->getOrderById($id);
        return new JsonModel($order->getLineItems());
    }

    /**
     * This method inspects the request and routes the data
     * to the correct method
     * This can be moved to a business model
     *
     * @return void
     */
    public function create($data)
    {
        $items = $this->getLineItemsModel()->create($data);
        return new JsonModel($items);
    }

    /**
     * Will update the line item
     * TODO: Model will be better
     *
     * @return void
     */
    public function update($id, $data)
    {
        $lineItem = $this->getLineItemsModel()->update($id, $data);
        return new JsonModel($lineItem);
    }
    
    /**
     * (non-PHPdoc)
     * 
     * @see \Zend\Mvc\Controller\AbstractRestfulController::delete()
     */
    public function delete($id)
    {
        $productId = $this->params()->fromRoute('item');
        $lineItem = $this->getLineItemsModel()->delete($id,$productId);
        return new JsonModel($lineItem->toArray());
    }
    
    
    /**
     * Return the orders model
     *
     * @return \Orders\Model\Order
     */
    public function getOrdersModel()
    {
        if (! $this->orders) {
            $this->orders = $this->getServiceLocator()->get('Orders\Model\Order');
        }
    
        return $this->orders;
    }
    
    /**
     * Return the orders model
     *
     * @return \Orders\Model\Order
     */
    public function getLineItemsModel()
    {
        if (! $this->lineItems) {
            $this->lineItems = $this->getServiceLocator()->get('Orders\Model\LineItem');
        }
    
        return $this->lineItemss;
    }
}