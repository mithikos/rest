<?php
namespace Orders\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="line_items", uniqueConstraints={@UniqueConstraint(name="lineitem", columns={"product_id", "order_id"})})
 * @Entity
 */
class LineItem
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $product_id;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $order_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Orders\Entity\Order", inversedBy="items")
     * @JoinColumn(name="order_id", referencedColumnName="order_id")
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="Products\Entity\Product", inversedBy="orders")
     * @JoinColumn(name="product_id", referencedColumnName="product_id")
     */
    protected $product;

    /**
     *
     * @return Products\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Will increase the quantity
     *
     * @param unknown $quantity            
     */
    public function increaseQuantity($quantity)
    {
        $this->quantity += $quantity;
    }

    /**
     * 
     * @param Products\Entity\Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
        $this->product_id = $product->getId();
    }

    /**
     * 
     * @param Orders\Entity\Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
        $this->order_id = $order->getId();
    }

    /**
     * 
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    /**
     *
     * @return array
     */
    public function toArray()
    {
        if ($this->order === null && $this->product === null) {
            return array();
        }
        
        return array(
            "product" => $this->product->getName()
        );
    }
}