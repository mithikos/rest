<?php
namespace Orders\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use DoctrineORMModuleTest\Assets\Entity\Date;

/**
 * @ORM\Entity
 * @ORM\Table(name="orders")
 */
class Order
{

    const ORDER_DRAFT = 'DRAFT';

    const ORDER_PLACED = 'PLACED';

    const ORDER_PAID = 'PAID';

    const ORDER_CANCELLED = 'CANCELLED';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="order_id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="order_status", type="string")
     */
    protected $status;

    /**
     * @ORM\Column(name="order_date", type="datetime")
     */
    protected $oDate;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $vat;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $reason;

    /**
     * @ORM\OneToMany(targetEntity="Orders\Entity\LineItem", mappedBy="order")
     *
     * @var ArrayCollection
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->status = self::ORDER_DRAFT;
        $this->oDate = new \DateTime("now");
        $this->vat = 20.00;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return boolean
     */
    public function placed()
    {
        if ($this->items->count() > 0 && $this->status === self::ORDER_DRAFT) {
            $this->status = self::ORDER_PLACED;
            return true;
        }
        
        return false;
    }

    /**
     *
     * @return boolean
     */
    public function paid()
    {
        if ($this->status === self::ORDER_PLACED) {
            $this->status = self::ORDER_PAID;
            return true;
        }
        
        return false;
    }

    /**
     * Will cancel the order
     *
     * @return boolean
     */
    public function cancel()
    {
        if ($this->status === self::ORDER_PLACED || $this->status === self::ORDER_DRAFT) {
            $this->status = self::ORDER_CANCELLED;
            return true;
        }
        
        return false;
    }

    /**
     * The status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * The net total
     *
     * @return float
     */
    public function total()
    {
        $total = 0;
        if ($this->items->count() > 0) {
            foreach ($this->items as $item) {
                $total += (float) ($item->getProduct()->getPrice() * $item->getQuantity());
            }
        }
        return $total;
    }

    /**
     *
     * @return array
     */
    public function getLineItems()
    {
        $items = array();
        if ($this->items->count() > 0) {
            foreach ($this->items as $item) {
                $items[] = $item->getProduct()->getName();
            }
        }
        
        return $items;
    }

    /**
     * Will set the reason on cancelling
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            "order_id" => $this->id,
            "status" => $this->status,
            "order_date" => $this->oDate,
            "net" => $this->total(),
            "gross_total" => ($this->vat / 100) * $this->total() + $this->total(),
            "items" => $this->getLineItems()
        );
    }
}