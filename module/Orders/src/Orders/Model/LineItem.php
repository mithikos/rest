<?php
namespace Orders\Model;

class LineItem
{

    private $entityManager;

    public function __construct(Doctrine\ORM\EntityManager $em)
    {
        $this->entityManager = $this->entityManager;
    }

    /**
     * Will add a new item into an order
     *
     * @param array $data            
     * @throws \Exception
     */
    public function create($data)
    {
        if (! isset($data['order_id']) || ! isset($data['product_id']) || ! isset($data['quantity'])) {
            throw new \Exception("Order or Product is missing!", 404);
        }
        
        $order = $this->entityManager->find("Orders\Entity\Order", $data['order_id']);
        
        if (! $order) {
            throw new \Exception("Order not found!", 404);
        }
        
        $product = $this->entityManager->find("Products\Entity\Product", $data['product_id']);
        
        if (! $product) {
            throw new \Exception("Product not found", 404);
        }
        
        if ($data['quantity'] < 1) {
            throw new \Exception("Quantity need to be greater than 0.", 500);
        }
        
        $lineItem = $this->entityManager->find("Orders\Entity\LineItem", array(
            "order_id" => $data['order_id'],
            'product_id' => $data['product_id']
        ));
        
        if ($lineItem) {
            $lineItem->increaseQuantity($data['quantity']);
            $this->entityManager->persist($lineItem);
            $this->entityManager->flush();
        } else {
            $lineItem = new \Orders\Entity\LineItem();
            $lineItem->increaseQuantity($data['quantity']);
            $lineItem->setOrder($order);
            $lineItem->setProduct($product);
            $this->entityManager->persist($lineItem);
            $this->entityManager->flush();
        }
        
        return $order->toArray();
    }

    public function update($id, $data)
    {
        if (! isset($id) || ! isset($data['product_id']) || ! isset($data['quantity'])) {
            throw new \Exception("Order or Product is missing!", 404);
        }
        
        $order = $this->entityManager->find("Orders\Entity\Order", $id);
        
        if (! $order) {
            throw new \Exception("Order not found!", 404);
        }
        
        $product = $this->entityManager->find("Products\Entity\Product", $data['product_id']);
        
        if (! $product) {
            throw new \Exception("Product not found!", 404);
        }
        
        if ($data['quantity'] < 1) {
            throw new \Exception("Quantity need to be greater than 0.", 500);
        }
        
        $lineItem = $this->entityManager->find("Orders\Entity\LineItem", array(
            "order_id" => $id,
            'product_id' => $data['product_id']
        ));
        
        if (! $lineItem) {
            throw new \Exception("Product not found!", 404);
        }
        
        $lineItem->setQuantity($data['quantity']);
        $this->entityManager->persist($lineItem);
        $this->entityManager->flush();
        
        return $order->toArray();
    }

    /**
     *
     * @param int $id Order id
     * @param itn $productId product id
     * @throws \Exception
     * @return array
     */
    public function delete($id, $productId)
    {
        if (! isset($id) || ! isset($productId)) {
            throw new \Exception("Order or Product is missing!", 404);
        }
        
        $order = $this->entityManager->find("Orders\Entity\Order", $id);
        
        if (! $order) {
            throw new \Exception("Order not found!", 404);
        }
        
        $product = $this->entityManager->find("Products\Entity\Product", $productId);
        
        if (! $product) {
            throw new \Exception("Product not found!", 404);
        }
        
        $lineItem = $this->entityManager->find("Orders\Entity\LineItem", array(
            "order_id" => $id,
            'product_id' => $productId
        ));
        
        if (! $lineItem) {
            throw new \Exception("Product not found!", 404);
        }
        
        $this->entityManager->remove($lineItem);
        $this->entityManager->flush();
        
        return $order->toArray();
    }
}