<?php
namespace Orders\Model;

use Doctrine\ORM\EntityManager;

class Order
{

    /**
     * The entity manager
     *
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     *
     * @var array
     */
    private $availableStatus = array(
        "cancel",
        "placed",
        "paid"
    );

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Will get The order by id
     *
     * @param int $id            
     * @throws \Exception
     */
    public function getOrderById($id)
    {
        $order = $this->entityManager->find('Orders\Entity\Order', $id);
        if (! $order) {
            throw new \Exception("Order not found!", 404);
        }
        return $order->toArray();
    }

    /**
     * Will return an array of orders
     *
     * @param string $status            
     * @throws \Exception
     * @return array of orders
     */
    public function getOrdersByStatus($status)
    {
        if (! in_array($status, $this->availableStatus)) {
            throw new \Exception("Invalid status", 404);
        }
        
        $orders = $this->entityManager->getRepository('Orders\Entity\Order')->findBy(array(
            "status" => strtoupper($status)
        ));
        
        if (! $orders) {
            throw new \Exception("No orders under status {$status}", 404);
        }
        
        return $this->ordersToArray($orders);
    }

    /**
     * Will return all the orders
     * 
     * @return array
     */
    public function getAllOrders()
    {
        $orders = $this->entityManager->getRepository('Orders\Entity\Order')->findAll();
        return $this->ordersToArray($orders);
    }

    /**
     * Will take a array of entities and will transform it into array
     *
     * @param array $orders            
     * @return array
     */
    private function ordersToArray($orders)
    {
        if (count($orders) === 0) {
            throw new \Exception("No orders found!", 404);
        }
        
        $result = array();
        
        foreach ($orders as $order) {
            $result[] = $order->toArray();
        }
        
        return $result;
    }
    

    /**
     * Create an order
     * @param array $order
     * @throws Exception
     * @return array
     */
    public function create($order) {
        
        try {
            $order = new \Orders\Entity\Order();
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }catch(\Exception $ex) {
            throw $ex;       
        }
        finally {
            return $order->toArray();
        }        
    }
    
    /**
     * Update a record
     * 
     * TODO: Break it more to smaller functions
     * @param int $id
     * @param array $data
     * @throws \Exception
     */
    public function update($id,$data) {
        $order = $this->entityManager->find("Orders\Entity\Order", $id);
        
        if (! $order) {
            throw new \Exception("Order not found!",404);
        }
        
        if (!isset($data['status'])) {
            throw new \Exception("Status is required as parameter",500);
        }
        
        if (isset($data['status']) && ! in_array($data['status'], $this->availableStatus)) {
            throw new \Exception("Invalid status {$data['status']}",500);
        }
        
        if ($data['status'] === 'cancel' && ! isset($data['reason'])) {
            throw new \Exception("Reason is required in-case of cancelling",500);
        }
        
        if (isset($data['reason']) && $data['status'] === 'cancel') {
            $order->setReason($data['reason']);
        }
        
        $statusNow = $order->getStatus();
        $statusStr = strtolower($data['status']);
        $result = $order->{$statusStr}();
        
        if ($result === false) {
            throw new \Exception("You cannot change the status order from {$statusNow} to {$data['status']}",500);
        }
        
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        
        
        return $order->toArray();
    }
}