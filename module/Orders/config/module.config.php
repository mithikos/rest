<?php
use Orders\Model\Order;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'orders' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/orders[/id/:id][/status/:status]',
                    'defaults' => array(
                        'controller' => 'Orders\Controller\Index'
                    )
                )
            ),
            'items' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/items[/order/:id][/item/:item]',
                    'defaults' => array(
                        'controller' => 'Orders\Controller\LineItems'
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Orders\Controller\Index' => 'Orders\Controller\IndexController',
            'Orders\Controller\LineItems' => 'Orders\Controller\LineItemsController'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'order_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Orders/Entity'
                )
            ),
            
            'orm_default' => array(
                'drivers' => array(
                    'Orders\Entity' => 'order_entities'
                )
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'Orders\Model\Order' => function ($sm)
            {
                $em = $sm->get('Doctrine\ORM\EntityManager');
                $model = new \Orders\Model\Order($em);
                return $model;
            },
            'Orders\Model\LineItem' => function ($sm)
            {
                $em = $sm->get('Doctrine\ORM\EntityManager');
                $model = new \Orders\Model\LineItem($em);
                return $model;
            }
        )
    )
);