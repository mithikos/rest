<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'products' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/product[/:id]',
                    'defaults' => array(
                        'controller' => 'Products\Controller\Index'
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Products\Controller\Index' => 'Products\Controller\IndexController'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'product_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Products/Entity'
                )
            ),
            
            'orm_default' => array(
                'drivers' => array(
                    'Products\Entity' => 'product_entities'
                )
            )
        )
    ),
);