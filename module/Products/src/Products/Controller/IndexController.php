<?php
namespace Products\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

/**
 * This class is the responsible to answer the requests to the /wall endpoint
 *
 * @package Orders/Controller
 */
class IndexController extends AbstractRestfulController
{

    private $productsTable;

    /**
     * Method not available for this endpoint
     *
     * @return void
     */
    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $product = $em->find('Products\Entity\Product', $id);
        if (! $product) {
            return $this->notFoundAction();
        }
        return new JsonModel($product->toArray());
    }

    /**
     * Method not available for this endpoint
     *
     * @return void
     */
    public function getList()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $records = $em->getRepository('Products\Entity\Product')->findAll();
        
        $result = array();
        if ($records) {
            foreach ($records as $record) {
                $result[] = $record->toArray();
            }
        }
        
        return new JsonModel($result);
    }

    /**
     * This method inspects the request and routes the data
     * to the correct method
     *
     * @return void
     */
    public function create($data)
    {
        if (! isset($data['name']) || ! isset($data['price'])) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Name OR price of the product is missing!");
            return $this->response;
        }
        
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        
        $product = $em->getRepository("Products\Entity\Product")->findBy(array(
            "name" => trim($data['name'])
        ));
        
        if (! $product) {
            $product = new \Products\Entity\Product();
            $product->setName(trim($data['name']));
            $product->setPrice($data['price']);
            
            $em->persist($product);
            $em->flush();
            
            return new JsonModel($product->toArray());
        }
        
        $this->response->setStatusCode(500);
        $this->response->setContent("Name must be unique!");
        return $this->response;
    }

    /**
     * Method not available for this endpoint
     *
     * @return void
     */
    public function update($id, $data)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        
        $product = $em->find("Products\Entity\Product", $id);
        
        if (! $product) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Product cannot be found!");
            return $this->response;
        }
        
        if (isset($data['name'])) {
            $checkName = $em->getRepository("Products\Entity\Product")->findBy(array(
                "name" => trim($data['name'])
            ));
            if (! $checkName) {
                $product->setName(trim($data['name']));
            } else {
                $this->response->setStatusCode(500);
                $this->response->setContent("You need to define a new name or exclude the field from the request!");
                return $this->response;
            }
        }
        
        if (isset($data['price'])) {
            $product->setPrice($data['price']);
        }
        $em->persist($product);
        $em->flush();
        
        return new JsonModel($product->toArray());
        
        $this->response->setStatusCode(500);
        $this->response->setContent("Name must be unique!");
        return $this->response;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\Mvc\Controller\AbstractRestfulController::delete()
     */
    public function delete($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        
        $existOnOrders = $em->getRepository("Orders\Entity\LineItem")->findBy(array(
            "product_id" => $id
        ));
        
        if (! $existOnOrders) {
            $product = $em->find("Products\Entity\Product", $id);
            
            if (! $product) {
                $this->response->setStatusCode(404);
                $this->response->setContent("Cannot find product!");
                return $this->response;
            }
            
            $em->remove($product);
            $em->flush();
            $this->response->setStatusCode(200);
            $this->response->setContent("success");
            return $this->response;
        }
        
        $this->response->setStatusCode(500);
        $this->response->setContent("Cannot be deleted when is bind to an order!");
        return $this->response;
    }
}